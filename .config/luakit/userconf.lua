local editor = require "editor"
local formfiller = require "formfiller"
local gopher = require "gopher"
local modes = require "modes"
local msg = require "msg"
local select = require "select"
local settings = require "settings"
local window = require "window"

editor.editor_cmd = "i3-sensible-terminal -e \"vim {file} +{line}\""

formfiller.extend({
	pass = function(s) return io.popen("pass " .. s):read() end,
})

-- A lot of website use `<Escape>` to close dialogs, so only `<Control>-[` will
-- leave passthrough mode
modes.remove_binds("passthrough", {"<Escape>"});

-- use `<control-w>` for closing tabs
--modes.remove_binds("normal", {"d"})

modes.add_binds("normal", {
	-- better hotkey for passthrough
	{"<control-p>", function(w) w:set_mode("passthrough") end},
	-- clipboard
	{"<control-c>", function() luakit.selection.clipboard = luakit.selection.primary end}
})

-- use home row for hinting
--[[
select.label_maker = function()
	--local chars = charset("dfghjkl")
	local chars = interleave("sdfg", "hjkl")
	return sort(reverse(chars))
end
]]--

-- use left hand for hinting
select.label_maker = function()
	local chars = interleave("qwaszx", "erdfcv")
	return sort(reverse(chars))
end

-- custom search engines
--   an "s" suffix normally means search; a missing "s" suffix means go there directly
local search_engines = settings.get_setting("window.search_engines")
search_engines.crate = "https://crates.io/crates/%s"
search_engines.crates = "https://crates.io/search?q=%s"
search_engines.librs = "https://lib.rs/search?q=%s"
search_engines.py = "https://docs.python.org/3/library/%s"
search_engines.pys = "https://docs.python.org/3/search.html?q=%s"
search_engines.pip = "https://pypi.org/search/?q=%s"
search_engines.youtube = "https://www.youtube.com/results?search_query=%s"
