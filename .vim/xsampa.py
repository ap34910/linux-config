#!/bin/python

DEBUG = False

import csv
from functools import reduce
from pathlib import Path
import re
import shelve
import sys

def file_is_newer(path1, path2):
    path1_mod = path1.stat().st_mtime
    path2_mod = path2.stat().st_mtime
    return path1_mod > path2_mod

def get_tsv_path():
    script_dir = Path(__file__).parent.resolve()
    xsampa_tsv = script_dir / "xsampa.tsv"
    return xsampa_tsv

def get_shelf_path():
    script_dir = Path(__file__).parent.resolve()
    xsampa_shelf = script_dir / "xsampa.shelf"
    return xsampa_shelf

def get_xsampa():
    xsampa_tsv = get_tsv_path()
    xsampa_shelf = get_shelf_path()
    if not xsampa_tsv.exists():
        raise RuntimeError
    if not DEBUG and xsampa_shelf.exists() and file_is_newer(xsampa_shelf, xsampa_tsv):
        #print("using existing shelf")
        with shelve.open(str(xsampa_shelf)) as shelf:
            d = {k: shelf[k] for k in shelf}
            return d
    #print("recomputing shelf")
    with open(xsampa_tsv, encoding="utf-8") as f:
        mapping = {}
        reader = csv.DictReader(filter(lambda row: row and row[0] != "#", f), delimiter="\t", quoting=csv.QUOTE_NONE)
        for row in reader:
            mapping[row["xsampa"]] = row["ipa"]
    regex = list(map(re.escape, mapping.keys()))
    #print(regex)
    d = {"mapping": mapping, "regex": regex}
    with shelve.open(str(xsampa_shelf)) as shelf:
        for k in d:
            shelf[k] = d[k]
    return d

def get_xsampa_regex():
    return get_xsampa()["regex"]

def get_xsampa_mapping():
    return get_xsampa()["mapping"]

def _match_reduce(acc, m):
    if acc:
        if m.start() < acc.start() or m.start() == acc.start() and len(m[0]) > len(acc[0]):
            return m
        else:
            return acc
    else:
        return m

def convert_xsampa(xsampa):
    mapping = get_xsampa_mapping()
    regex = get_xsampa_regex()
    out = ""
    while xsampa:
        m = reduce(_match_reduce, filter(bool, map(lambda k: re.search(re.escape(k), xsampa), mapping.keys())), None)
        if m:
            out += xsampa[:m.start()]
            out += mapping[m[0]]
            xsampa = xsampa[m.end():]
        else:
            out += xsampa
            xsampa = ""
    return out

if __name__ == "__main__":
    for line in sys.stdin:
        print(convert_xsampa(line))
