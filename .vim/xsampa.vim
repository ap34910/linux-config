function XSampa()
	let result = trim(system("~/.vim/xsampa.py", input("XSampa: ")))
	let cmd = printf("normal i%s", result)
	execute cmd
endfunction

command XSampa call XSampa()

nmap <C-X> :XSampa<CR>
" this is an overwrite
imap <C-X> <C-O>:XSampa<CR>
