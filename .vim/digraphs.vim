" Utilities for examining currently defined digraphs
function SaveDigsFunc()
	redir! > ~/.vim/all_digraphs.txt
	silent digraphs!
	redir END
	tabe ~/.vim/all_digraphs.txt
	echo "digraphs have been saved to ~/.vim/all_digraphs.txt"
endfunction

command SaveDigs call SaveDigsFunc()



" New digraphs

" U+018F LATIN CAPITAL LETTER SCHWA
digraph EI 399
" U+0259 LATIN SMALL LETTER SCHWA
digraph eI 601
" U+0283 LATIN SMALL LETTER ESH
digraph sH 643
" U+0292 LATIN SMALL LETTER EZH (overwrite)
digraph zH 658
" U+0294 LATIN LETTER GLOTTAL STOP
digraph gl 660
" U+27e8 (MATHEMATICAL LEFT ANGLE BRACKET)
digraph </ 10216
" U+27e9 (MATHEMATICAL RIGHT ANGLE BRACKET)
digraph /> 10217
