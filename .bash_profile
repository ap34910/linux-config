#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

export EDITOR=/bin/nvim

eval `keychain --agents ssh --eval id_gitlab --clear`
